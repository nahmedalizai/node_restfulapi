const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');
const UsersController = require('../controllers/users');

router.get('/',checkAuth, UsersController.get_all_users);

router.post('/signup', UsersController.add_new_user);

router.post('/login', UsersController.verify_user);

router.delete('/:userId',checkAuth, UsersController.delete_user);

module.exports = router;