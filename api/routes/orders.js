const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');
const OrdersController = require('../controllers/orders');

router.get('/',checkAuth, OrdersController.get_all_orders);

router.get('/:orderId', checkAuth, OrdersController.get_order_by_id);

router.post('/', checkAuth, OrdersController.place_new_order);

router.patch('/:orderId', checkAuth, OrdersController.modify_order);

router.delete('/:orderId', checkAuth, OrdersController.delete_order);

module.exports = router;