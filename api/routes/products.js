const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');
const ProductsController = require('../controllers/products');
//arg0 : URL (i.e. /products), arg1: object

router.get('/', ProductsController.get_all_products);

router.get('/:productId', ProductsController.get_product_by_id);

router.post('/',checkAuth, ProductsController.add_new_product);

router.patch('/:productId', checkAuth, ProductsController.modify_product);

router.delete('/:productId',checkAuth, ProductsController.delete_product);

module.exports = router;