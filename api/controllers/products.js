const mongoose = require('mongoose');
const Product = require('../models/product');

exports.get_all_products = (req, res, next) => {
    Product.find()
        .exec()
        .then(docs => {
            console.log(docs);
            res.status(200).json(docs);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
}

exports.get_product_by_id = (req, res, next) => {
    const id = req.params.productId;
    Product.findById(id)
        .exec()
        .then(doc => {
            console.log(doc);
            if(doc)
                res.status(200).json(doc);
            else
                res.status(404).json("No results found");
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
}

exports.add_new_product = (req, res, next) => {
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price
    });
    //promise
    product
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json(result);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
}

exports.modify_product = (req, res, next) => { // Request body must be a JSON array for this method e.g. [ { "propName" : "price", "value": "1"},{ "propName" : "name", "value": "new name"}]
    const id = req.params.productId;
    const updateOps = {};
    for(const ops of req.body)
        updateOps[ops.propName] = ops.value;
    Product.update({_id:id}, {$set: updateOps})
    .exec()
    .then(result => {
        console.log(result);
        res.status(200).json(result);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});
    });
}

exports.delete_product = (req, res, next) => {
    const id = req.params.productId;
    Product.remove({_id:id})
        .exec()
        .then(result => {
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
}