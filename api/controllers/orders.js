const mongoose = require('mongoose');
const Order = require('../models/order');

exports.get_all_orders = (req, res, next) => {
    Order.find()
    .select('product quantity _id')
    .populate('product', 'name')
    .exec()
    .then(docs => {
        res.status(200).json({
            count: docs.length,
            orders: docs.map(doc => {
                return {
                    _id: doc._id,
                    productId: doc.product,
                    quantity: doc.quantity,
                    request: {
                        type: 'GET',
                        URL: 'http://localhost:3300/orders/' + doc._id
                    }
                }
            })
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});
    });
}

exports.get_order_by_id = (req, res, next) => {
    Order.findById(req.params.orderId)
    .populate('product')
    .exec()
    .then(order => {
        if(order)
            res.status(200).json(order);
        else
            res.status(404).json({message: "Order not found"});
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});
    });
}

exports.place_new_order = (req, res, next) => {
    const order = new Order({
        _id: mongoose.Types.ObjectId(),
        quantity: req.body.quantity,
        product: req.body.productId
    });
    order
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json(result);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
}

exports.modify_order = (req, res, next) => { // json array required in request body and orderId in params
    const id = req.params.orderId;
    const updateOps = {};
    for(const ops of req.body)
        updateOps[ops.propName] = ops.value;
    Order.update({_id:id}, {$set: updateOps})
        .exec()
        .then(result => {
            console.log(result);
            res.status(200).json(result); //TODO: more meaningful message here
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
}

exports.delete_order = (req, res, next) => {
    Order.remove({_id: req.params.orderId})
    .exec()
    .then(order => {
        res.status(200).json(order);  //TODO: more meaningful message here
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});
    });
}
