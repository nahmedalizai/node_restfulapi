const express = require('express');
const app = express();
const morgan = require('morgan');
const productRoutes = require('./api/routes/products');
const orderRoutes = require('./api/routes/orders');
const userRoutes = require('./api/routes/users');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//Mongodb client using mongoose package
//mongoose.connect('mongodb+srv://node-restful:<password>@node-restful-xcg81.mongodb.net/test?retryWrites=true&w=majority');
mongoose.connect(
    'mongodb+srv://node-restful:' + 
    process.env.MONGO_ATLAS_PW + 
    '@node-restful-xcg81.mongodb.net/test?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);
mongoose.Promise = global.Promise;

// log error in console in dev env using morgan
app.use(morgan('dev'));

//requests parser
app.use(bodyParser.urlencoded({extended: false})); // for url encoded data only extended is set to false
app.use(bodyParser.json()); // allow parsing (extracting) json data

// Allowing Cross-Origin requests
app.use((req, res, next) =>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    if(req.method === 'OPTIONS') { // browsers options response header
        res.header(
            'Access-Control-Allow-Methods', 
            'PUT, POST, PATCH, DELETE, GET'
        ); 
        return res.status(200).json({});
    }
    next();
});

// Default path
app.get('/', (request, response) => {
    response.json({ info: 'Node.js and Express Restful API'})
  });

// Request handling routes
app.use('/products',productRoutes);
app.use('/orders',orderRoutes);
app.use('/users',userRoutes);

// Error handling for the unknown paths
app.use((req,res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req,res, next) => {
    res.status(error.status || 500);
    res.json({
        error:{
            message : error.message
        }
    });
});

module.exports = app;